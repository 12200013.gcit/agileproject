const mongoose=require('mongoose')
const validator=require('validator')
const bcrypt=require('bcryptjs')

const userSchema=new mongoose.Schema({
    name: {
        type: String,
        require: [true, 'Please tell us your name'],
    },
    email: {
        type: String,
        required: [true, 'Please tell use your email'],
        unique: true,
        lowercase:true,
        validate:[validator.isEmail, 'please provide a valid email']
    },
    photo: {
        type:String,
        default: 'default.jpg'
    },
    role:{
        type: String,
        enum: ['user','sme','pharmacist','admin'],
        default: 'user'
    },
    password: {
        type: String,
        required: [true, 'please provide a password'],
        minlength: 8,
        select: false
    },
    active: {
        type: Boolean,
        default: true,
        select: false
    },
    passwordConfirm: {
        type: String,
        required: [true, 'please confirm your password'],
        validate: {
            validator: function(el) {
                return el === this.password;
            },
            message: 'Passwords are not the same',
        },
    },
})
userSchema.pre('findOneAndUpdate', async function (next){
    // only run this function if password was actually modified
    if(update.password!=='' && update.password!==undefined && update.password==update.passwordConfirm){

        // hash the password with cost of 12
        this.getUpdate().password=await bcrypt.hash(update.password,12)

        // delete passwordConfirm field
        update.passwordConfirm=undefined
        next()
    }else
    next()
})
userSchema.methods.correctPassword=async function (
    candidatePassword,
    userPassword,
){
    return await bcrypt.compare(candidatePassword,userPassword)
}
const User=mongoose.model('User',userSchema)
module.exports=User
