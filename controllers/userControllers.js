const User=require('./../model/userModels')

exports.getAllusers=async(req,res,next)=>{
    console.log("getting all users");
    try{
        const users=await User.find()
        res.status(200).json({data:users,status: 'success'})
    }catch(err){
        res.status(500).json({error:"from get all user"});
    }
}

exports.createUser=async(req,res)=>{
    try{
        const user=await User.create(req.body);
        console.log(req.body.name)
        res.json({data:user,status:'success'});
    }catch(err){
        res.status(500).json({error: err.message});
    }
}

exports.getUser=async(req,res)=>{
    try{
        const user=await User.findById(req.params.id);
        res.json({data:user, status:'success'});
    }catch(err){
        res.status(500).json({error:err.message});
    }
}

exports.updataUser=async(req,res) =>{
    try{
        const user =await User.findByIdAndUpdate(req.params.id, req.body);
        res.json({data:user,status:'success'});
    }catch(err){
        res.status(500).json({error:err.message});
    }
}

exports.deleteUser=async(req,res) =>{
    try{
        const user =await User.findByIdAndDelete(req.params.id);
        res.json({data:user,status:'success'});
    }catch(err){
        res.status(500).json({error:err.message});
    }
}