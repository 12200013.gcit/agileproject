const express=require('express')
const userController=require('./../controllers/userControllers')
const authController=require('./../controllers/authController')
const router=express.Router()

router.post('/signip',authController.signup)
router.post('/login',authController.login )

router 
    .route('/')
    .get(userController.getAllusers)
    .post(userController.createUser)

router
    .route('/:id')
    .get(userController.getAllusers)
    .patch(userController.updataUser)
    .delete(userController.deleteUser)

module.exports=router