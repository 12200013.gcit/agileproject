const mongoose=require('mongoose')
const dotenv = require('dotenv')
dotenv.config()
const app = require('./app');

const LOCAL_DB = process.env.DATABASE_LOCAL.replace(
    'PASSWORD',
    process.env.DATABASE_PASSWORD
)
const DB = process.env.DATABASE.replace(
    "PASSWORD",
    process.env.DATABASE_PASSWORD
)

mongoose.connect(DB)
.then((con) => {
    console.log("Successfully connected");
})
.catch(error => console.log(error))

const port=8001;

app.listen(port, () =>{
    console.log(`app running on port ${port}..`)
})