const express = require('express');
const path=require('path')
const app=express();

app.use(express.json())
const userRouter=require('./routes/userRoute')
const viewRouter=require('./routes/viewRoutes')

app.use('/api/v1/users', userRouter)
app.use('/', viewRouter)

app.use(express.static(path.join(__dirname, 'views')))

module.exports=app;
