class AppError extends Error{// inheriting from the parent class Error
    constructor(message,statusCode){//object will take the message and the stautscode
        // call parent constructor using super 
        super(message)//message is the paramete that the built-in error accepts

        this.statusCode=statusCode
        this.stutus=`${statusCode}`.startsWith('4') ? 'fail':'error'
        this.isOperational=true

        Error.captureStackTrace(this,this.constructor)
    }
}
module.exports=AppError